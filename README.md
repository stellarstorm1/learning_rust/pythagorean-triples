# Pythagorean Triples

Calculate the first 448 "largest" Pythagorean triples where x is <= 448, and
y and z are within the range [3, 100000]

Precalculated values are provided in [first_448.csv](first_448.csv)

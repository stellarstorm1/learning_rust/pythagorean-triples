fn main() {
    let mut root_range = Vec::new();
    let mut sq_range = Vec::new();
    for i in 3i128..100_000i128 {
        root_range.push(i);
        sq_range.push(i.pow(2));
    }

    for (i, x) in sq_range.iter().enumerate() {
        let mut array: [i128; 3] = [0; 3];
        for (j, y) in sq_range.iter().enumerate() {
            for (k, z) in sq_range.iter().enumerate() {
                if x + y == *z {
                    // Overwrite on every successful iteration so that only the
                    // "biggest" triple for the given x is kept
                    array[0] = root_range[i];
                    array[1] = root_range[j];
                    array[2] = root_range[k];
                }
            }
        }
        let diff: i128 = array[2] - array[1];
        if array != [0; 3] {
            for bit in &array {
                print!("{}, ", bit);
            }
            println!("{}", diff);
        }
        if diff > 2 {
            break;
        }
    }
}
